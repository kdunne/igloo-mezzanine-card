EESchema Schematic File Version 4
LIBS:igloo-mezzanine-card-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gbtx:igloo U?
U 5 1 5DF72521
P 2750 3250
F 0 "U?" H 2775 3315 50  0000 C CNN
F 1 "igloo" H 2775 3224 50  0000 C CNN
F 2 "" H 2750 3250 50  0001 C CNN
F 3 "" H 2750 3250 50  0001 C CNN
	5    2750 3250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
