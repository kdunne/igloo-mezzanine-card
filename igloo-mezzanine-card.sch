EESchema Schematic File Version 4
LIBS:igloo-mezzanine-card-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 6300 2700 1800 1650
U 5DF71D01
F0 "IGLOO Power" 50
F1 "igloo-power.sch" 50
$EndSheet
$Sheet
S 9050 2700 1550 1650
U 5DF7208A
F0 "IGLOO JTAG" 50
F1 "igloo-jtag.sch" 50
$EndSheet
$Sheet
S 2250 1500 2250 4200
U 5DF5A04C
F0 "IGLOO banks" 50
F1 "igloo-banks.sch" 50
F2 "VCC1V8" I L 2250 1600 50 
F3 "VCC2V5" I L 2250 1750 50 
F4 "GND" I L 2250 1900 50 
F5 "GBTX_TMS_REM_P" I L 2250 2250 50 
F6 "GBTX_TMS_REM_N" I L 2250 2350 50 
F7 "GBTX_TDI_REM_P" I L 2250 2500 50 
F8 "GBTX_TDI_REM_N" I L 2250 2600 50 
F9 "GBTX_RST_REM_P" I L 2250 2750 50 
F10 "GBTX_RST_REM_N" I L 2250 2850 50 
F11 "GBTX_TCK_REM_P" I L 2250 3000 50 
F12 "GBTX_TCK_REM_N" I L 2250 3100 50 
F13 "GBTX_KEY_A_P" I L 2250 4500 50 
F14 "GBTX_KEY_A_N" I L 2250 4400 50 
F15 "GBTX_TMS_LOC_P" I L 2250 3250 50 
F16 "GBTX_TMS_LOC_N" I L 2250 3350 50 
F17 "GBTX_TDI_LOC_P" I L 2250 3500 50 
F18 "GBTX_TDI_LOC_N" I L 2250 3600 50 
F19 "GBTX_RST_LOC_P" I L 2250 3750 50 
F20 "GBTX_RST_LOC_N" I L 2250 3850 50 
F21 "GBTX_TCK_LOC_P" I L 2250 4000 50 
F22 "GBTX_TCK_LOC_N" I L 2250 4100 50 
F23 "GBTX_KEY_B_P" I L 2250 4650 50 
F24 "GBTX_KEY_B_N" I L 2250 4750 50 
F25 "GBTX_RXRDY" I L 2250 4950 50 
F26 "GBTX_RXDATAVALID" I L 2250 5100 50 
F27 "IGLOO_TMS_REM" O R 4500 1850 50 
F28 "IGLOO_TDI_REM" O R 4500 2000 50 
F29 "IGLOO_RST_REM" O R 4500 2150 50 
F30 "IGLOO_TCK_REM" O R 4500 2300 50 
F31 "IGLOO_TMS_LOC" O R 4500 2500 50 
F32 "IGLOO_TDI_LOC" O R 4500 2650 50 
F33 "IGLOO_RST_LOC" O R 4500 2800 50 
F34 "IGLOO_TCK_LOC" O R 4500 2950 50 
$EndSheet
Text Label 1400 2250 0    50   ~ 0
IGLOO_TMS_REM_P
Text Label 1400 2350 0    50   ~ 0
IGLOO_TMS_REM_N
Text Label 1400 2500 0    50   ~ 0
IGLOO_TDI_REM_P
Text Label 1400 2600 0    50   ~ 0
IGLOO_TDI_REM_N
Text Label 1400 2750 0    50   ~ 0
IGLOO_TDI_REM_N
$EndSCHEMATC
