EESchema Schematic File Version 4
LIBS:igloo-mezzanine-card-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gbtx:igloo U?
U 6 1 5DF72C7D
P 5050 3200
F 0 "U?" H 5528 2646 50  0000 L CNN
F 1 "igloo" H 5528 2555 50  0000 L CNN
F 2 "" H 5050 3200 50  0001 C CNN
F 3 "" H 5050 3200 50  0001 C CNN
	6    5050 3200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
